﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Proyecto
{
    public partial class Productos : System.Web.UI.Page
    {
        SqlConnection Conn = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\luisa\\Documents\\Visual Studio 2015\\Projects\\Proyecto\\BaseDatos_PoleZone.mdf; Integrated Security=True;Connect Timeout=30");
        //VANESSA -> "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=D:\\Users\\Luis\\Documents\\Visual Studio 2015\\Projects\\Proyecto\\BaseDatos_PoleZone.mdf; Integrated Security=True;Connect Timeout=30"
        //NEWTON -> "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\luisa\\Documents\\Visual Studio 2015\\Projects\\Proyecto\\BaseDatos_PoleZone.mdf; Integrated Security=True;Connect Timeout=30"
        Int16 idProducto = 0;
        String horaCalculo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Session["horaCalculo"] != null)
            {
                horaCalculo = (String)this.Session["horaCalculo"];
                labelHora.Text = "A las " + horaCalculo + " estimaste tu costo de clases. Aquí hay otras cosas que te podrían ser útiles:";
            }

            String OrderSql = String.Format("SELECT * FROM productosPoleZone");
            using (Conn)
            {
                using (SqlCommand cmd = new SqlCommand(OrderSql, Conn))
                {
                    Conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    DataTable dt = new DataTable();
                    dt.Load(dr);
                    productGrid.DataSource = dt; //ID GRID VIEW
                    productGrid.DataBind();
                }
            }
            Conn.Close();
        }

        protected void ButtonVenta_Click(object sender, EventArgs e)
        {
            Conn.ConnectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\luisa\\Documents\\Visual Studio 2015\\Projects\\Proyecto\\BaseDatos_PoleZone.mdf; Integrated Security=True;Connect Timeout=30";
            try
            {
                Conn.Open();
                String OrderSql = "vendeProducto";
                SqlCommand cmd = new SqlCommand(OrderSql, Conn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.CommandText = "vendeProducto";
                cmd.Parameters.Add(new SqlParameter("@idProducto", idProducto));

                SqlParameter sp = new SqlParameter();
                sp.ParameterName = "@countStock";
                sp.SqlDbType = SqlDbType.Int;
                sp.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(sp);

                cmd.ExecuteNonQuery();
                
                if (int.Parse(sp.Value.ToString()) > 0)
                {
                    TextBoxIDProducto.Text = "";
                    Console.WriteLine("Se actualiza con procedimiento");
                }
                else
                {
                    Console.WriteLine("No existe la clave....");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error de actualizacion: " + ex.Message);
            }
            Conn.Close();
            Response.Redirect(Request.RawUrl);
        }

        protected void TextBoxIDProducto_TextChanged(object sender, EventArgs e)
        {
            idProducto = Convert.ToInt16(TextBoxIDProducto.Text);
        }
    }
}