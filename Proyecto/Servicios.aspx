﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Servicios.aspx.cs" Inherits="Proyecto.Servicios" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<header>
    <link rel="stylesheet" type="text/css" href="styles.css">
</header>
<head runat="server">
    <title>Pole Zone - Servicios</title>
    <script language="javascript">
        function detectCiudad(sender, args) {
            if (args.Value.toLowerCase() == "merida" || args.Value.toLowerCase() == "mérida") {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }
    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <appsettings>
        <add key ="ValidationSetttings:UnobtrusiveValidationMode" value="None"/>
    </appsettings>
</head>
<body>
    <div id="navBar">
        <div>
            <img id="logo_topIzq" src="assets/pole_logo.png">
        </div>

        <div>
            <nav>
                <ul id="pagePicker">
                    <li>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Index.aspx">Inicio</asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Clases.aspx">Clases</asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/Alumnos.aspx">Alumnos</asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Productos.aspx">Productos</asp:HyperLink></li>
                    <li id="currentPage">
                        <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Servicios.aspx">Servicios</asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Login.aspx">Cerrar Sesión</asp:HyperLink></li>
                </ul>
            </nav>
        </div>
    </div>

    <div id="content">
        <h2>¡Tipos de clase!</h2>
        <h2>Pole Fitness:</h2>
        <img src="assets/pole_fitness.png" style="max-width: 20%" />
        <h2>TRX:</h2>
        <img src="assets/trx.png" style="max-width: 20%" />
        <h2>¡Encuentra dónde estamos ubicados!</h2>
        <form id="form1" runat="server">
            <h2>Ciudad:&nbsp;<asp:TextBox ID="TextBoxLocation" runat="server" OnTextChanged="TextBoxLocation_TextChanged"></asp:TextBox>
                <asp:CustomValidator ID="CustomValidatorLocation" runat="server" ClientValidationFunction="detectCiudad" ControlToValidate="TextBoxLocation" ErrorMessage="¡Eso no es Mérida!"></asp:CustomValidator>
            </h2>
            <h2>Código postal:&nbsp;
                    <asp:TextBox ID="TextBoxZIP" runat="server" OnTextChanged="TextBoxZIP_TextChanged"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidatorZIP" runat="server" ControlToValidate="TextBoxZIP" ErrorMessage="No es un Código Postal" ValidationExpression="\d{5}"></asp:RegularExpressionValidator>
            </h2>
            <h2>
                <asp:Button ID="ButtonLocate" runat="server" OnClick="ButtonLocate_Click" Text="¡Encuéntrame!" />
                <asp:Label ID="LabelConfirma" runat="server"></asp:Label>
            </h2>
        </form>
    </div>
</body>
</html>
