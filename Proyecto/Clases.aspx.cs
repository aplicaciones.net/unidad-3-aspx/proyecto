﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto
{
    public partial class Clases : System.Web.UI.Page
    {
        SqlConnection Conn = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\luisa\\Documents\\Visual Studio 2015\\Projects\\Proyecto\\BaseDatos_PoleZone.mdf; Integrated Security=True;Connect Timeout=30");
        //VANESSA -> "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=D:\\Users\\Luis\\Documents\\Visual Studio 2015\\Projects\\Proyecto\\BaseDatos_PoleZone.mdf; Integrated Security=True;Connect Timeout=30"
        //NEWTON -> "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\luisa\\Documents\\Visual Studio 2015\\Projects\\Proyecto\\BaseDatos_PoleZone.mdf; Integrated Security=True;Connect Timeout=30"

        Int16 numClases = 0;
        HttpCookie numClasesCookie;

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;

            String OrderSql = String.Format("SELECT * FROM horariosPoleZone");
            using (Conn)
            {
                using (SqlCommand cmd = new SqlCommand(OrderSql, Conn))
                {
                    Conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    DataTable dt = new DataTable();
                    dt.Load(dr);
                    scheduleGrid.DataSource = dt; //ID GRID VIEW
                    scheduleGrid.DataBind();
                }
            }
            Conn.Close();
        }

        protected void ButtonCalcular_Click(object sender, EventArgs e)
        {

        }

        protected void TextBoxCantClases_TextChanged(object sender, EventArgs e)
        {
            if (TextBoxCantClases.Text != null)
            {
                numClases = Convert.ToInt16(TextBoxCantClases.Text);
                this.ViewState["numClases"] = numClases;
                this.Session["horaCalculo"] = DateTime.Now.ToString("H:mm");
                numClasesCookie = new HttpCookie("numClasesCookie", Convert.ToString(numClases));
                numClasesCookie.Expires = DateTime.Now.AddSeconds(5);
                this.Response.Cookies.Add(numClasesCookie);
                calcularClases();
            }
        }
        private void calcularClases()
        {
            numClases = (Int16)this.ViewState["numClases"];
            Decimal costo = numClases * 100 - numClases*10;
            labelCosto.Text = "$" + Convert.ToString(costo) + " por " + this.ViewState["numClases"] + " clases!";
        }
    }
}
