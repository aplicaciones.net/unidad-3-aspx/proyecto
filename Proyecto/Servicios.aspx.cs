﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto
{
    public partial class Servicios : System.Web.UI.Page
    {
        String state, zip;
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
        }

        protected void ButtonLocate_Click(object sender, EventArgs e)
        {
            if (state == "Merida" || zip == "97203")
            {
                LabelConfirma.Text = "¡Si te quedamos cerca! Busca en Plaza Banderas";
            } else
            {
                LabelConfirma.Text = "No te quedamos tan cerca, estamos ubicados en Mérida, Yucatán. En la colonia Vista Alegre Norte.";
            }
        }

        protected void TextBoxLocation_TextChanged(object sender, EventArgs e)
        {
            state = Convert.ToString(TextBoxLocation.Text);
        }

        protected void TextBoxZIP_TextChanged(object sender, EventArgs e)
        {
            zip = Convert.ToString(TextBoxZIP.Text);
        }
    }
}