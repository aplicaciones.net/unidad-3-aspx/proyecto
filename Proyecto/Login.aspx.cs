﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Proyecto
{
    public partial class Login : System.Web.UI.Page
    {
        String user, pass, repeat_pass;
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (user == "Admin" && pass == "privado" && repeat_pass == "privado")
            {
                this.Response.Redirect("Index.aspx");
            }
        }

        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {
            pass = Convert.ToString(campoPass.Text);
        }

        protected void repitePass_TextChanged(object sender, EventArgs e)
        {
            repeat_pass = Convert.ToString(repitePass.Text);
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            user = Convert.ToString(campoUser.Text);
        }
    }
}