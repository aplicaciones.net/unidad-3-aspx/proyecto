﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Productos.aspx.cs" Inherits="Proyecto.Productos" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<header>
    <link rel="stylesheet" type="text/css" href="styles.css">
</header>
<head runat="server">
    <title>Pole Zone - Productos</title>
</head>
<body>
    <div id="navBar">
        <div>
            <img id="logo_topIzq" src="assets/pole_logo.png">
            <nav>
                <ul id="pagePicker">
                    <li>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Index.aspx">Inicio</asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Clases.aspx">Clases</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/Alumnos.aspx">Alumnos</asp:HyperLink>
                    </li>
                    <li id="currentPage">
                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Productos.aspx">Productos</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Servicios.aspx">Servicios</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Login.aspx">Cerrar Sesión</asp:HyperLink>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div id="content">

        <form runat="server">
            <h2>¡Inventario!</h2>
            <asp:GridView ID="productGrid" runat="server"></asp:GridView>
            <br />
            <asp:TextBox ID="TextBoxIDProducto" runat="server" ToolTip="Id de Producto" OnTextChanged="TextBoxIDProducto_TextChanged"></asp:TextBox>
            <br />
            <asp:Button ID="ButtonVenta" runat="server" Text="Vender" OnClick="ButtonVenta_Click" />
        </form>

        <h2 style="margin-top: 75px;">¡Para que las clases sean más amenas!</h2>
        <asp:Label ID="labelHora" runat="server" Text="" Style="font-size: 30px"></asp:Label>
        <br />
        <img src="assets/producto1.png" style="max-width: 20%; margin-bottom: 5px" />
        <h2>Evita resbalarte con esta increíble crema para manos, ¡ya no más manos sudorosas!</h2>
        <br />
        <img src="assets/producto2.png" style="max-width: 20%; margin-bottom: 5px" />
        <h2>¡Tops y licras variadas!</h2>
        <br />
        <img src="assets/producto3.png" style="max-width: 20%; margin-bottom: 5px" />
        <h2>No pierdas condición por faltar unas clases, ¡continúa haciendo ejercicios en casa con estas pelotas!</h2>
        <br />
        <img src="assets/producto4.jpg" style="max-width: 20%; margin-bottom: 5px" />
        <h2>¿Te resulta incómodo hacer ejercicios en el suelo? Ya no más con estos tapetes de yoga</h2>
    </div>
</body>
</html>
