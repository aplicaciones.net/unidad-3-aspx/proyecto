﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;

namespace Proyecto
{
    public partial class Alumnos : System.Web.UI.Page
    {
        SqlConnection Conn = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\luisa\\Documents\\Visual Studio 2015\\Projects\\Proyecto\\BaseDatos_PoleZone.mdf; Integrated Security=True;Connect Timeout=30");
        //VANESSA -> "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=D:\\Users\\Luis\\Documents\\Visual Studio 2015\\Projects\\Proyecto\\BaseDatos_PoleZone.mdf; Integrated Security=True;Connect Timeout=30"
        //NEWTON -> "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\luisa\\Documents\\Visual Studio 2015\\Projects\\Proyecto\\BaseDatos_PoleZone.mdf; Integrated Security=True;Connect Timeout=30"


        String numClases = "";
        HttpCookie numClasesCookie;

        String dataNewNombre;
        String dataNewApellido;
        int valueNewPole, valueNewTrx, valueNewTwerk;

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
            numClases = Convert.ToString(this.Session["numClases"]);
            numClasesCookie = this.Request.Cookies["numClasesCookie"];
            if (numClasesCookie != null)
            {
                LabelClasesDetectadas.Text = "Detectamos que elegiste " + numClasesCookie.Value + " clases al mes. ¡Estos serían tus compañeros!";
                Label1.Text = "Luis Azcuaga, 27 años - TRX";
                Label2.Text = "Michelli Góngora, 20 años - Pole Fitness";
                Label3.Text = "Fernanda Mena, 18 años - Pole Fitness";
                Label4.Text = "Javier González, 22 años - TRX";
            }

            String OrderSql = String.Format("SELECT * FROM alumnosPoleZone");
            using (Conn)
            {
                using (SqlCommand cmd = new SqlCommand(OrderSql, Conn))
                {
                    Conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    DataTable dt = new DataTable();
                    dt.Load(dr);
                    alumnosGrid.DataSource = dt; //ID GRID VIEW
                    alumnosGrid.DataBind();
                }
            }
            Conn.Close();
        }

        protected void Button1_Click(object sender, EventArgs e) //Insert into alumnos
        {
            SqlConnection Conn = new SqlConnection();
            Conn.ConnectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\luisa\\Documents\\Visual Studio 2015\\Projects\\Proyecto\\BaseDatos_PoleZone.mdf; Integrated Security=True;Connect Timeout=30";
            if (dataNewNombre != "" && dataNewApellido != "")
            {
                SqlDataAdapter adp = null;
                DataSet ds; // Arreglo de objetos
                try
                {
                    Conn.Open();
                    String OrdenSql = "SELECT * FROM alumnosPoleZone";
                    ds = new DataSet();
                    adp = new SqlDataAdapter(OrdenSql, Conn);
                    adp.Fill(ds, "CopiaBD");
                    DataRow dr;
                    dr = ds.Tables["CopiaBD"].NewRow();

                    dr["nombreAlumno"] = dataNewNombre;
                    dr["apellidoAlumno"] = dataNewApellido;
                    dr["clasesPole"] = valueNewPole;
                    dr["clasesTwerk"] = valueNewTwerk;
                    dr["clasesTRX"] = valueNewTrx;

                    ds.Tables["CopiaBD"].Rows.Add(dr);
                    // Vaciamos la informacion a la Tabla
                    SqlCommandBuilder cb;
                    cb = new SqlCommandBuilder(adp);
                    adp.Update(ds.Tables["CopiaBD"]);

                    TextBoxNombre.Text = "";
                    TextBoxApellido.Text = "";
                    DropDownListPole.SelectedIndex = 0;
                    DropDownListTWERK.SelectedIndex = 0;
                    DropDownListTRX.SelectedIndex = 0;
                    //Button2.Enabled = true;
                    Response.Redirect(Request.RawUrl);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Conn.Close();
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            SqlConnection Conn = new SqlConnection();
            Conn.ConnectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\luisa\\Documents\\Visual Studio 2015\\Projects\\Proyecto\\BaseDatos_PoleZone.mdf; Integrated Security=True;Connect Timeout=30";
            Int16 lastId = 0;
            try
            {
                Conn.Open();
                String OrdenSql = String.Format("SELECT top 1 Id FROM alumnosPoleZone order by Id desc");
                SqlCommand cmd = new SqlCommand(OrdenSql, Conn);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        lastId = Convert.ToInt16(String.Format("{0}", reader["Id"]));
                    }
                }
                OrdenSql = String.Format("DELETE FROM alumnosPoleZone WHERE Id = {0}", lastId);
                cmd = new SqlCommand(OrdenSql, Conn);
                cmd.ExecuteNonQuery();
                //Button2.Enabled = false;
                Response.Redirect(Request.RawUrl);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Mensaje de error: " + ex.Message);
            }
            Conn.Close();
        }

        protected void TextBoxNombre_TextChanged(object sender, EventArgs e)
        {
            dataNewNombre = Convert.ToString(TextBoxNombre.Text);
        }

        protected void TextBoxApellido_TextChanged(object sender, EventArgs e)
        {
            dataNewApellido = Convert.ToString(TextBoxApellido.Text);
        }

        protected void DropDownListPole_SelectedIndexChanged(object sender, EventArgs e)
        {
            valueNewPole = Convert.ToInt16(DropDownListPole.SelectedValue);
        }
        
        protected void DropDownListTWERK_SelectedIndexChanged(object sender, EventArgs e)
        {
            valueNewTwerk = Convert.ToInt16(DropDownListTWERK.SelectedValue);
        }

        protected void DropDownListTRX_SelectedIndexChanged(object sender, EventArgs e)
        {
            valueNewTrx = Convert.ToInt16(DropDownListTRX.SelectedValue);
        }
    }
}