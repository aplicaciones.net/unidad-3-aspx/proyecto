﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Proyecto.Login" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" type="text/css" href="styles.css" />
    <title>Login</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <appsettings>
        <add key ="ValidationSetttings:UnobtrusiveValidationMode" value="None"/>
    </appsettings>
</head>
<body>
    <p style="text-align: center">
        <asp:Image ID="Image1" runat="server" src="assets/pole_logo.png" Style="max-width: 50%; margin-bottom: 0px;" />

        <div id="content" style="margin-top:10%">
            <form id="form1" runat="server">
                <label>Usuario </label>
                <asp:TextBox ID="campoUser" runat="server" type="text" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
                <label>Contraseña </label>
                <asp:TextBox ID="campoPass" runat="server" type="password" OnTextChanged="TextBox2_TextChanged"></asp:TextBox>
                <asp:TextBox ID="repitePass" runat="server" type="password" OnTextChanged="repitePass_TextChanged"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;
	    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Entrar" />
                <br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorUser" runat="server" ControlToValidate="campoUser" ErrorMessage="Es necesario poner usuario."></asp:RequiredFieldValidator>
                &nbsp;&nbsp;&nbsp;
            <asp:CompareValidator ID="CompareValidatorPass" runat="server" ControlToCompare="repitePass" ControlToValidate="campoPass" ErrorMessage="La contraseña no es la misma"></asp:CompareValidator>
            </form>
        </div>
    </p>
</body>
</html>
