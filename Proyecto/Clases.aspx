﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Clases.aspx.cs" Inherits="Proyecto.Clases" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<header>
    <link rel="stylesheet" type="text/css" href="styles.css">
</header>
<head runat="server">
    <title>Pole Zone - Clases</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <appsettings>
		<add key ="ValidationSetttings:UnobtrusiveValidationMode" value="None"/>
	</appsettings>
</head>
<body>
    <div id="navBar">
        <div>
            <img id="logo_topIzq" src="assets/pole_logo.png">
        </div>

        <nav>
            <ul id="pagePicker">
                <li>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Index.aspx">Inicio</asp:HyperLink></li>
                <li id="currentPage">
                    <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Clases.aspx">Clases</asp:HyperLink>
                </li>
                <li>
                    <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/Alumnos.aspx">Alumnos</asp:HyperLink>
                </li>
                <li>
                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Productos.aspx">Productos</asp:HyperLink>
                </li>
                <li>
                    <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Servicios.aspx">Servicios</asp:HyperLink>
                </li>
                <li>
                    <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Login.aspx">Cerrar Sesión</asp:HyperLink>
                </li>
            </ul>
        </nav>
    </div>
    </div>
    <div id="content">
        <h1>¡Horario! </h1>
        <form runat="server">
            <asp:GridView ID="scheduleGrid" runat="server"></asp:GridView>

            <div style="margin-left: 15%; background-color: #00CCCC33; margin-right: 15%; border-radius: 15px">
                <h2>¡Calculadora clases! </h2>
                <h3>¿Cuántas clases quieres tomar? </h3>
                <asp:TextBox ID="TextBoxCantClases" runat="server" OnTextChanged="TextBoxCantClases_TextChanged"></asp:TextBox>&nbsp;
			        <asp:RangeValidator ID="RangeValidatorClases" runat="server" ControlToValidate="TextBoxCantClases" ErrorMessage="¡Mínimo 1 clase y máximo 20!" MaximumValue="20" MinimumValue="1" Type="Integer"></asp:RangeValidator>
                <br />
                <br />
                <asp:Button ID="ButtonCalcular" runat="server" OnClick="ButtonCalcular_Click" Text="¡Calcular!" />
                <asp:Label ID="labelCosto" runat="server" Text="$ -" Style="float: right; margin-right: 20%; font-size: 30px; color: red"></asp:Label>
                <br />
                <br />
                <br />
                <br />
        </form>
    </div>
    </div>
</body>
</html>
