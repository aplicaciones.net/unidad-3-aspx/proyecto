﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Proyecto.Index" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<header>
    <link rel="stylesheet" type="text/css" href="styles.css">
</header>
<head runat="server">
    <title>Pole Zone - Inicio</title>
</head>
<body>
    <div id="navBar">
        <div>
            <a href="easterEgg.aspx">
                <img id="logo_topIzq" src="assets/pole_logo.png">
            </a>
        </div>

        <div>
            <nav>
                <ul id="pagePicker">
                    <li id="currentPage">
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Index.aspx">Inicio</asp:HyperLink></li>
                    <li>
                        <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Clases.aspx">Clases</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/Alumnos.aspx">Alumnos</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Productos.aspx">Productos</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Servicios.aspx">Servicios</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Login.aspx">Cerrar Sesión</asp:HyperLink>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div id="content">
        <h1>Qui&eacute;nes somos</h1>
        <h2 style="text-align: center;">¡Atrévete a probar algo diferente! Toma una clase de Pole Fitness y enamórate</h2>
        <img src="assets/banner.png" style="max-width: 70%" />
    </div>
</body>
</html>
