﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Alumnos.aspx.cs" Inherits="Proyecto.Alumnos" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<form id="form1" runat="server">
    <header>
        <link rel="stylesheet" type="text/css" href="styles.css">
    </header>
    <head runat="server">
        <title>Pole Zone - Alumnos</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <appsettings>
         <add key = "ValidationSetttings:UnobtrusiveValidationMode" value = "None"/>
       </appsettings>
    </head>
    <body>
        <div id="navBar">
            <div>
                <img id="logo_topIzq" src="assets/pole_logo.png">
            </div>

            <div>
                <nav>
                    <ul id="pagePicker">
                        <li>
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Index.aspx">Inicio</asp:HyperLink></li>
                        <li>
                            <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Clases.aspx">Clases</asp:HyperLink>
                        </li>
                        <li id="currentPage">
                            <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/Alumnos.aspx">Alumnos</asp:HyperLink>
                        </li>
                        <li>
                            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Productos.aspx">Productos</asp:HyperLink>
                        </li>
                        <li>
                            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Servicios.aspx">Servicios</asp:HyperLink>
                        </li>
                        <li>
                            <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Login.aspx">Cerrar Sesión</asp:HyperLink>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div id="content">
            <h2>¡Actualmente inscritos!</h2>
            <asp:Label ID="LabelClasesDetectadas" runat="server" Text="" Style="font-size: 30px"></asp:Label>
            <ol>
                <asp:Label ID="Label1" runat="server" Text="Luis" Style="font-size: 25px"></asp:Label>
                <br />
                <asp:Label ID="Label2" runat="server" Text="Michelli" Style="font-size: 25px"></asp:Label>
                <br />
                <asp:Label ID="Label3" runat="server" Text="Fernanda" Style="font-size: 25px"></asp:Label>
                <br />
                <asp:Label ID="Label4" runat="server" Text="Javier" Style="font-size: 25px"></asp:Label>
                <br />
                <br />
            </ol>
            <asp:TextBox ID="TextBoxNombre" runat="server" OnTextChanged="TextBoxNombre_TextChanged" ToolTip="Nombre alumno"></asp:TextBox>
            <asp:TextBox ID="TextBoxApellido" runat="server" OnTextChanged="TextBoxApellido_TextChanged" ToolTip="Apellido alumno"></asp:TextBox>

            <asp:DropDownList ID="DropDownListPole" runat="server" OnSelectedIndexChanged="DropDownListPole_SelectedIndexChanged" ToolTip="Clases Pole">
                <asp:ListItem>0</asp:ListItem>
                <asp:ListItem>4</asp:ListItem>
                <asp:ListItem>8</asp:ListItem>
                <asp:ListItem>12</asp:ListItem>
                <asp:ListItem>16</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
            </asp:DropDownList>


            <asp:DropDownList ID="DropDownListTWERK" runat="server" OnSelectedIndexChanged="DropDownListTWERK_SelectedIndexChanged" ToolTip="Clases Twerk">
                <asp:ListItem>0</asp:ListItem>
                <asp:ListItem>4</asp:ListItem>
                <asp:ListItem>8</asp:ListItem>
                <asp:ListItem>12</asp:ListItem>
                <asp:ListItem>16</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
            </asp:DropDownList>

            <asp:DropDownList ID="DropDownListTRX" runat="server" OnSelectedIndexChanged="DropDownListTRX_SelectedIndexChanged" ToolTip="Clases TRX">
                <asp:ListItem>0</asp:ListItem>
                <asp:ListItem>4</asp:ListItem>
                <asp:ListItem>8</asp:ListItem>
                <asp:ListItem>12</asp:ListItem>
                <asp:ListItem>16</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Añadir Alumno" />
            <br />
            <asp:Button ID="Button2" runat="server" Text="Deshacer último alumno" OnClick="Button2_Click" />
            <asp:GridView ID="alumnosGrid" runat="server"></asp:GridView>
        </div>
</form>
</body>
</html>
